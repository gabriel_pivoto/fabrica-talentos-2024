// Nomenclatura de variáveis

const Users = [
  {
    title: 'User',
    followers: 5
  },
  {
    title: 'Friendly',
    followers: 50
  },
  {
    title: 'Famous',
    followers: 500
  },
  {
    title: 'Super Star',
    followers: 1000
  }
]

export default async function getUserNameAndTitle(request, response) {
  const userNameGitHub = String(request.query.username)

  if (userNameGitHub == false) {
    return response.status(400).json({
      message: `Please provide an username to search on the github API`
    })
  }

  const userResponse = await fetch(`https://api.github.com/users/${userNameGitHub}`);

  if (userResponse.status === 404) {
    return response.status(400).json({
      message: `User with username "${userNameGitHub}" not found`
    })
  }

  const data = await userResponse.json()

  const orderUsers = Users.sort((less, more) =>  more.followers - less.followers); 

  const category = orderUsers.find( UserByfollowersIndex => data.followers > UserByfollowersIndex.followers)

  const result = {
    userNameGitHub,
    category: category.title
  }

  return result
}

getUserNameAndTitle({ query: {
  username: 'josepholiveira'
}}, {})